Assign 1a: add w/h and h/w to the resize code in glOrtho()
Assign 1b: add angle as 0.0f in gluPerspecive()
Assign 2a: model transfomation of triangle using glTranslatef()
Assign 2b: view transformation of triangle using gluLookAt()
Assign 3:  show a multicolored triangle and cornflower quad in same window with black background
Assign 4:  Rotate single multicolored triangle and place the rotation call(i.e. create a function named update and write the rotation code inside it) inside game loop before display()
Assign 5:  Use assignment 3 as is and rotate both the shapes (multicolored triangle along y-axis and corflower quad along x-axis)
Assign 6:  Spin deathly hallows around Y-axis